FROM python:3.8
COPY src .
COPY requirements.txt .
ENV URL "localhost"
ENV PORT "5000"
ENV KEY "keykey"
RUN pip install -r requirements.txt
EXPOSE 5000
CMD ["python3.8","-m", "main","$URL", "$KEY"]