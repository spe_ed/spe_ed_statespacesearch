<!--
*** Build using the Best-README-Template.
-->

<!-- PROJECT LOGO -->
<br />
<p align="center">
![SPE_ED](spe_ed_logo_side.png "SPE_ED Automat")
  <h3 align="center">SPE_ED State Space Search Agent in Python (deprecated)</h3>

  <p align="center">
    Implementation of an Agent for the game spe_ed using State Space Search.<br />
    This is a project developed for the 2021 InformatiCup
    <br />
    <a href="https://gitlab.gwdg.de/spe_ed/spe_ed_statespacesearch/issues">Report Bug</a>
    ·
    <a href="https://gitlab.gwdg.de/spe_ed/spe_ed_statespacesearch/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
  </ol>
</details>

### Built With

<div style="display: -ms-flexbox;     display: -webkit-flex;     display: flex;     -webkit-flex-direction: row;     -ms-flex-direction: row;     flex-direction: row;     -webkit-flex-wrap: wrap;     -ms-flex-wrap: wrap;     flex-wrap: wrap;     -webkit-justify-content: space-around;     -ms-flex-pack: distribute;     justify-content: space-around;     -webkit-align-content: stretch;     -ms-flex-line-pack: stretch;     align-content: stretch;     -webkit-align-items: flex-start;     -ms-flex-align: start;     align-items: flex-start;">
<a href="https://kotlinlang.org/"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/768px-Python-logo-notext.svg.png" alt="Kotlin" width="64" height="64" title="Kotlin"></a>
<a href="https://www.docker.com/"><img src="https://www.docker.com/sites/default/files/d8/2019-07/vertical-logo-monochromatic.png" alt="Docker" width="64" height="64" title="Docker"></a>
</div>


<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites
* [Docker](https://docs.docker.com/get-docker/)

### Installation

1. Clone the repo
  ```sh
  git clone https://gitlab.gwdg.de/spe_ed/spe_ed_statespacesearch.git
  ```
2. Update submodules:
```
git submodule update --init --recursive
cd spe_ed_game
git checkout master
```
3. Use Docker to build the image
  ```sh
  docker build -t spe_ed_agent .
  ```
4. Use Docker to run the image
  ```sh
  docker run -e URL="<URL>" -e KEY="<API key>" -e TIME_URL="<TIME URL>" spe_ed_agent
  ```



<!-- USAGE EXAMPLES -->
## Usage

Use the `.env` file to set custom game url, if the offical servers aren't available and
 ```sh
docker-compose up --build
```
to build and start a new container with the env values overwritten

To start multiple players use
```sh
docker-compose up --scale client=<player number> --build
```

<!-- CONTACT -->
## Contact

Luca Francis - luca.francis24@gmail.com

Project Link: [https://gitlab.gwdg.de/spe_ed/spe_ed_statespacesearch](https://gitlab.gwdg.de/spe_ed/spe_ed_statespacesearch)

