import datetime
import json

from automat.cell import Cell
from game.playerInfo import PlayerInfo


class GameState:
    width: int
    height: int
    cells: [[Cell]]
    players: [PlayerInfo]
    you: int
    running: bool
    deadline: datetime

    def toJson(self):
        return json.dumps(self.__dict__)

    def printBoard(self):
        for height in range(self.height):
            for width in range(self.width):
                if self.cells[height][width].value != 0:
                    value = self.cells[height][width].value
                else:
                    value = "."
                print(value, end="\t")
            print("")
        print("")

    def __init__(self, width: int, height: int, cells: [[int]], players: [PlayerInfo], you: int, running: bool,
                 deadline: datetime):
        self.width = width
        self.height = height
        self.cells = []
        for height, row in enumerate(cells, 0):
            # print("height: ", height)
            _row = []
            for width, value in enumerate(row, 0):
                # print("width: ", width)
                _row.append(Cell(width, height, value))
            self.cells.append(_row)
        if(width < 20):
            self.printBoard()
        self.players = players
        self.you = you
        self.running = running
        self.deadline = deadline
