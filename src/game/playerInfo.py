import json

from automat.cell import Cell
from game.directions import Direction


class PlayerInfo:
    position: Cell
    direction: Direction
    speed: int
    active: bool
    name: str

    def toJson(self):
        return json.dumps(self.__dict__)

    def __init__(self, x: int, y: int, dir: str, speed: int, active: bool, name: str):
        self.position = Cell(x, y, -1)
        self.direction = Direction(dir)
        self.speed = speed
        self.active = active
        self.name = name

