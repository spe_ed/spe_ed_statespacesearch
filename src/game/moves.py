from enum import Enum


class Move(Enum):
    turn_left = "turn_left"
    turn_right = "turn_right"
    speed_up = "speed_up"
    slow_down = "slow_down"
    change_nothing = "change_nothing"
