import json
import os

import websockets

from automat.automat import Automat
from game.gameState import GameState
from game.playerInfo import PlayerInfo


class WebSocketClient:

    def __init__(self):
        pass

    async def connect(self):
        '''
            Connecting to webSocket server

            websockets.client.connect returns a WebSocketClientProtocol, which is used to send and receive messages
        '''
        uri = "ws://" + os.environ['URL'] + ":" + os.environ['PORT'] + "?key=" + os.environ['KEY']
        self.connection = await websockets.client.connect(uri)
        if self.connection.open:
            print('Connection stablished. Client correcly connected')
            return self.connection

    async def sendMessage(self, message):
        '''
            Sending message to webSocket server
        '''
        await self.connection.send(message)

    async def handleGame(self, connection):
        '''
            Receiving all server messages and handling them
        '''
        game_round = 0
        agent = Automat()
        while True:
            try:
                message = await connection.recv()
                # print('Received message from server: ' + str(message))
                # Calculate Move here

                gameState: GameState = json.loads(message, object_hook=object_decoder)

                if gameState.players[(gameState.you + 1).__str__()].active:
                    move = agent.makeMove(gameState, game_round)
                    game_round = game_round + 1
                    await self.sendMessage(move)
            except websockets.exceptions.ConnectionClosed:
                print('Connection with server closed')
                break


def object_decoder(obj):
    if "cells" in obj:
        return GameState(obj['width'], obj['height'], obj['cells'], obj['players'], obj['you'], obj['running'],
                         obj['deadline'])
    if "name" in obj:
        return PlayerInfo(obj['x'], obj['y'], obj['direction'], obj['speed'], obj['active'], obj['name'])
    return obj
