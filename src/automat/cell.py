class Cell:
    x: int
    y: int
    value: int = 0

    def __init__(self, x, y, value):
        self.x = x
        self.y = y
        self.value = value

    def __str__(self):
        return "("+str(self.x)+","+str(self.y)+":"+str(self.value)+")"

    def copy(self):
        return Cell(self.x, self.y, self.value)
