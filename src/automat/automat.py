from operator import itemgetter
from typing import Union

import numpy as np

from automat.cell import Cell
from game.directions import Direction
from game.gameState import GameState
from game.moves import Move


class Automat:
    width: int
    height: int

    # EvalMove is calls 5^(future_steps), cant be > 1000
    future_steps = 4

    def __init__(self):
        pass

    def makeMove(self, gamestate: GameState, game_round: int) -> str:
        player = gamestate.players[(gamestate.you + 1).__str__()]
        self.width = gamestate.width
        self.height = gamestate.height
        possible_moves = []
        for move in Move:
            possible_moves.append(
                (move,
                 self.evalMove(player.position,
                               move,
                               player.speed,
                               player.direction,
                               gamestate.cells,
                               game_round,
                               self.future_steps))
            )

        # Array of index of best moves with same score
        best_move = max(possible_moves, key=itemgetter(1))
        # for tuple in possible_moves:
        # print(tuple[0].value, ":", tuple[1], end="\t\t")
        # print("")
        # print(best_move[0].value, ":", best_move[1])
        # print(player.speed)
        # print(game_round)
        # gamestate.printBoard()
        return best_move[0].value

    def evalMove(self,
                 cell: Cell,
                 move: Move,
                 speed: int,
                 direction: Direction,
                 cells: [[Cell]],
                 game_round: int,
                 future_steps: int) -> Union[np.ndarray, int, float]:

        # Adjust speed
        _speed = speed
        if move == Move.speed_up:
            _speed = _speed + 1
        if move == Move.slow_down:
            _speed = _speed - 1

        # Die if speed is invalid
        if _speed <= 0 or _speed > 10:
            return 0

        #Adjust direction
        _direction = direction
        if move == Move.turn_left:
            _direction = direction.toLeftTurn()
        if move == Move.turn_right:
            _direction = direction.toRightTurn()


        # Get new position and cells that were driven to get to that position
        new_position, new_cells = self.getAccessibleCellForDir(cell, _speed, _direction,
                                                               cells,
                                                               (game_round + 1))

        # Die if new position is invalid
        if new_position is None:
            return 0

        # Put cells onto board
        for new_cell in new_cells:
            cells[new_cell.y][new_cell.x].value = new_cell.value

        # Get score for current position
        evaluation = self.staticEval(new_position, cells)


        # Get scores for moves
        possible_moves_scores = []
        score = 0
        if future_steps != 0:
            for _move in Move:
                pms = self.evalMove(new_position,
                                    _move,
                                    _speed,
                                    _direction,
                                    cells,
                                    game_round + 1,
                                    future_steps - 1)
                possible_moves_scores.append(pms)
            # Use score of best move
            score = np.nanmax(possible_moves_scores)

        # Remove cells onto board
        for new_cell in new_cells:
            cells[new_cell.y][new_cell.x].value = 0

        return evaluation + score

    def getAccessibleCellForDir(self, cell: Cell, speed: int, direction: Direction, cells: [Cell],
                                game_round: int):
        position = cell.copy()
        new_cells = []
        jump = False

        for i in range(speed):
            stepsize = 1
            if game_round % 6 == 0 and speed >= 3:
                if jump:
                    stepsize = speed - 1
            if direction == Direction.up:
                position.y -= stepsize
            elif direction == Direction.right:
                position.x += stepsize
            elif direction == Direction.down:
                position.y += stepsize
            elif direction == Direction.left:
                position.x -= stepsize
            # player outside bounds -> dead
            if self.isOutsideField(position):
                return None, new_cells
            # player on already occupied cell -> dead
            if cells[position.y][position.x].value != 0:
                return None, new_cells
            # update current cell
            new_cells.append(position.copy())
            if game_round % 6 == 0 and speed >= 3:
                if jump:
                    break
                jump = True
        return position, new_cells

    def isOutsideField(self, position: Cell) -> bool:
        return (position.x < 0 or position.x >= self.width or position.y >= self.height or
                position.y < 0)

    def staticEval(self, cell: Cell, cells: [Cell]) -> int:
        return self.getNumberOfAvailableCellsInKernel(cell, cells, 6)

    def getAllCellsInKernel(self, cell: Cell, cells: [Cell], size: int) -> [[Union[Cell, None]]]:
        # Kernel is 2*size + 1
        values = []
        for row, a in enumerate(range(-size, size + 1), 0):
            values.append([])
            for b in range(-size, size + 1):
                try:
                    if cell.y + a >= 0 and cell.x + b >= 0:
                        values[row].append(cells[cell.y + a][cell.x + b])
                    else:
                        raise IndexError
                except IndexError:
                    values[row].append(None)
        return values

    def getNumberOfAvailableCellsInKernel(self, cell: Cell, cells: [Cell], size: int) -> [Union[int, None]]:
        values = self.getAllCellsInKernel(cell, cells, size)
        groups = []
        mappings = []

        # First Pass
        for y, row in enumerate(values, 0):
            group_id = 0
            groups.append([])
            for x, _cell in enumerate(row, 0):
                try:
                    # cell is wall
                    if _cell != values[size][size] and (_cell is None or _cell.value != 0):
                        raise IndexError

                    _groupId = None
                    left = None
                    down = None

                    try:
                        if groups[y - 1][x] is not None:
                            down = groups[y - 1][x]
                    except IndexError:
                        pass
                    try:
                        if groups[y][x - 1] is not None:
                            left = groups[y][x - 1]
                    except IndexError:
                        pass

                    if down is not None and left is not None:
                        _groupId = min([left, down])
                        if left != down:
                            mappings.append((max([left, down]), min([left, down])))
                    elif down is not None:
                        _groupId = down
                    elif left is not None:
                        _groupId = left
                    else:
                        group_id = group_id + 1
                        _groupId = group_id

                    groups[y].append(_groupId)
                except IndexError:
                    groups[y].append(None)

        # Second pass
        for high, low in mappings:
            for y, row in enumerate(groups, 0):
                for x, group in enumerate(row, 0):
                    if group is not None and group == high:
                        groups[y][x] = low

        amountOfEmptyAccessible = 0
        groupOfPosition = groups[size][size]
        for y, row in enumerate(groups, 0):
            for x, group in enumerate(row, 0):
                # if group is None:
                #     print("N", end="\t")
                # else:
                #     print("", end="\t")
                if group == groupOfPosition:
                    amountOfEmptyAccessible = amountOfEmptyAccessible + 1
        #     print("")
        # print("")
        # print(amountOfEmptyAccessible)
        return amountOfEmptyAccessible
